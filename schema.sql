-- Drop if exists for testing/dev purpose
DROP DATABASE IF EXISTS ece356_recipes_g29;

-- Setup database for tables
CREATE DATABASE ece356_recipes_g29;

-- Drop if exists for testing/dev purpose
DROP USER IF EXISTS 'recipes_29'@'localhost';

-- Create default user
CREATE USER 'recipes_29'@'localhost' IDENTIFIED BY 'ece356';
GRANT ALL PRIVILEGES ON ece356_recipes_g29.* TO 'recipes_29'@'localhost';
FLUSH PRIVILEGES;

-- Create Views and Tables
USE ece356_recipes_g29;

CREATE Table Users (
    id int NOT NULL AUTO_INCREMENT,
    first_name varchar(31), 
    middle_initial char(2),
    last_name varchar(31),
    email varchar(63),
    profile_pic varchar(255),
    display_name varchar(63),
    date_joined DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
);

CREATE TABLE RecipeGroup (
    id int NOT NULL AUTO_INCREMENT,
    group_name varchar(25),

    PRIMARY KEY (id)
);

CREATE Table Recipes (
    id int NOT NULL AUTO_INCREMENT,
    user_id int NOT NULL,
    recipe_group_id int,
    recipe_name varchar(255) NOT NULL,
    photo varchar(255),
    prep_time int,
    cook_time int,
    total_time int,
    recipe_description varchar(255),
    
    PRIMARY KEY (id),
    FOREIGN KEY (user_id)
        REFERENCES Users(id)
        ON DELETE CASCADE,

    FOREIGN KEY (recipe_group_id)
        REFERENCES RecipeGroup(id)
        ON DELETE CASCADE
);

CREATE TABLE Reviews (
    id int NOT NULL AUTO_INCREMENT,
    reviewer_id int NOT NULL,
    recipe_id int NOT NULL,
    rating int NOT NULL,
    review varchar(255),
    review_date DATETIME DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY (id, reviewer_id, recipe_id),
    FOREIGN KEY (reviewer_id)
        REFERENCES Users(id)
        ON DELETE CASCADE,
    FOREIGN KEY (recipe_id)
        REFERENCES Recipes(id)
        ON DELETE CASCADE
);

CREATE TABLE Instruction (
    recipe_id int NOT NULL,
    instruction varchar(512) NOT NULL,
    step_number int NOT NULL,

    PRIMARY KEY (recipe_id, step_number),
    FOREIGN KEY (recipe_id)
        REFERENCES Recipes(id)
        ON DELETE CASCADE
);

CREATE TABLE Ingredients (
    id int NOT NULL AUTO_INCREMENT,
    ingredient_name varchar(255) NOT NULL UNIQUE,

    PRIMARY KEY (id)
);

CREATE TABLE IngredientMap (
    recipe_id int NOT NULL,
    ingredient_id int NOT NULL,
    quantity int,
    unit varchar(7),

    PRIMARY KEY (recipe_id, ingredient_id),
    FOREIGN KEY (recipe_id)
        REFERENCES Recipes(id)
        ON DELETE CASCADE,
    FOREIGN KEY (ingredient_id)
        REFERENCES Ingredients(id)
        ON DELETE CASCADE
);

CREATE TABLE NutritionalInformation (
    id int NOT NULL AUTO_INCREMENT,
    recipe_id int NOT NULL,
    calories int,
    protein int,
    carbohydrates int,
    fat int,
    cholestral int,
    sodium int,
    sugar int,

    PRIMARY KEY (id, recipe_id),
    FOREIGN KEY (recipe_id)
        REFERENCES Recipes(id)
        ON DELETE CASCADE
);

CREATE TABLE UserFollows (
    follower_id int NOT NULL,
    following_id int not null,

    PRiMARY KEY (follower_id, following_id),
    FOREIGN KEY (follower_id)
        REFERENCES Users(id)
        ON DELETE CASCADE,
    FOREIGN KEY (following_id)
        REFERENCES Users(id)
        ON DELETE CASCADE
);

CREATE TABLE RecipeGroupFollows (
    follower_id int NOT NULL,
    group_id int NOT NULL,

    PRIMARY KEY (follower_id, group_id),
    FOREIGN KEY (follower_id)
        REFERENCES Users(id)
        ON DELETE CASCADE,

    FOREIGN KEY (group_id)
        REFERENCES RecipeGroup(id)
        ON DELETE CASCADE
);

CREATE TABLE RecipeCategory (
    group_id int NOT NULL,
    recipe_id int NOT NULL,

    PRIMARY KEY (group_id, recipe_id),
    FOREIGN KEY (group_id)
        REFERENCES RecipeGroup(id)
        ON DELETE CASCADE,
    FOREIGN KEY (recipe_id)
        REFERENCES Recipes(id)
        ON DELETE CASCADE
);

CREATE TABLE Favourites (
    user int NOT NULL,
    recipe int NOT NULL,

    PRIMARY KEY (user, recipe),
    FOREIGN KEY (user)
        REFERENCES Users(id)
        ON DELETE CASCADE,
    FOREIGN KEY (recipe)
        REFERENCES Recipes(id)
        ON DELETE CASCADE
);

CREATE TABLE Likes (
    user int NOT NULL,
    recipe int NOT NULL,
    date_liked DATETIME DEFAULT CURRENT_TIMESTAMP,
    is_dislike boolean,

    PRIMARY KEY (user, recipe),
    FOREIGN KEY (user)
        REFERENCES Users(id)
        ON DELETE CASCADE,
    FOREIGN KEY (recipe)
        REFERENCES Recipes(id)
        ON DELETE CASCADE
);

-- Creating indexes
CREATE INDEX recipe_id_idx ON IngredientMap(recipe_id);
CREATE INDEX recipe_id_idx ON Instruction(recipe_id);
CREATE INDEX recipe_id_idx ON Likes(recipe);
CREATE INDEX review_idx ON Reviews(recipe_id, reviewer_id);