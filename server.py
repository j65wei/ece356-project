# ECE356 Group 29 Fall 2021

import mysql.connector
import settings

# This function executes a single query with no return values
def connectExecuteSQL(query):
    conn = mysql.connector.connect(
        user=settings.USER,
        password=settings.PASSWORD,
        host=settings.HOST,
        database=settings.DATABASE
    )
    cursor = conn.cursor(buffered=True)
    cursor.execute(query)
    conn.commit()
    conn.close()

# This function executes a single query for multiple row selection
def connectExecuteInsertReturnLastRowIDSQL(query):
    # try:
    conn = mysql.connector.connect(
        user=settings.USER,
        password=settings.PASSWORD,
        host=settings.HOST,
        database=settings.DATABASE
    )
    cursor = conn.cursor(buffered=True)
    cursor.execute(query)
    conn.commit()
    result = cursor.lastrowid
    conn.close()
    return result

# This function executes insertion/replace queries returning an ID
def connectExecuteReturnFetchAll(query):
    conn = mysql.connector.connect(
        user=settings.USER,
        password=settings.PASSWORD,
        host=settings.HOST,
        database=settings.DATABASE
    )
    cursor = conn.cursor(buffered=True)
    cursor.execute(query)
    conn.commit()
    result = cursor.fetchall()
    conn.close()
    return result

### ALL USER RELATED QUERIES ###

# SQL Code to follow a user given a follower and folowee
def follow_user(follower_id, following_id):
    followQuery = """INSERT INTO UserFollows (follower_id, following_id) VALUES ({}, {});""".format(follower_id, following_id)
    connectExecuteSQL(followQuery)

# Server code to add a new user given information
def add_user(fname, mname, lname, email, pfp, displayname):
    createUserQuery = """
        INSERT INTO Users (
            first_name, 
            middle_initial, 
            last_name, 
            email,
            profile_pic, 
            display_name
        )
        VALUES (
            "{}",
            "{}",
            "{}",
            "{}",
            "{}",
            "{}"
        )
    """.format(fname, mname, lname, email, pfp, displayname)
    return connectExecuteInsertReturnLastRowIDSQL(createUserQuery)

# SQL Code to get information on a user by id
def get_user_by_id(id):
    fetchUserQuery = """SELECT * FROM Users WHERE id = {};""".format(id)
    res = connectExecuteReturnFetchAll(fetchUserQuery)
    if (len(res) == 0):
        return None
    return res
    
# SQL Code to Search user given a property
def search_user(prop, val):
    fetchUserQuery = """SELECT * FROM Users WHERE {} LIKE '%{}%' ORDER BY first_name ASC;""".format(prop, val)
    res = connectExecuteReturnFetchAll(fetchUserQuery)
    if (len(res) == 0):
        return None
    return res

# SQL Code to list users followed
def get_followed_users(user_id):
    fetchUserQuery = """
        SELECT Users.first_name, Users.middle_initial, Users.last_name, Users.display_name
        FROM UserFollows LEFT JOIN Users ON UserFollows.following_id = Users.id
        WHERE UserFollows.follower_id = {};
    """.format(user_id)
    res = connectExecuteReturnFetchAll(fetchUserQuery)
    if (len(res) == 0):
        return None
    return res
### ALL INGREIDENT RELATED QUERIES ###

# Helper function to insert a recipe
def add_ingredient(recipe_id, ingredient, quantity, unit):
    findIngredientIDQuery = """
        SELECT id FROM Ingredients WHERE ingredient_name="{}";
    """.format(ingredient)
    iresult = connectExecuteReturnFetchAll(findIngredientIDQuery)
    if len(iresult) == 0:
        insertIngredientsQuery = """
            INSERT INTO Ingredients (
                ingredient_name
            ) VALUES (
                "{}"
            )
        """.format(ingredient)
        ingredientid = connectExecuteInsertReturnLastRowIDSQL(insertIngredientsQuery)
    else:
        ingredientid = iresult[0][0]

    findIngredientQuery = """
        SELECT * FROM IngredientMap WHERE recipe_id={} AND ingredient_id={};
    """.format(recipe_id, ingredientid)
    res = connectExecuteReturnFetchAll(findIngredientQuery)
    if (len(res) == 0):
        insertIngredientsQuery = """
            INSERT INTO IngredientMap (
                recipe_id,
                ingredient_id,
                quantity,
                unit
            ) VALUES (
                {},
                {},
                {},
                "{}"
            )
        """.format(recipe_id, ingredientid, quantity, unit)
        connectExecuteSQL(insertIngredientsQuery)
    else:
        print("Ingredient exists for this ingredient. Please go to edit recipe option to change this.")

# SQL Code to fetch an ingredients ID given a name
def get_ingredient_id_by_name(ingredient_name):
    ingredientIdQuery = """
        SELECT id FROM Ingredients WHERE ingredient_name="{}";
    """.format(ingredient_name)
    res = connectExecuteReturnFetchAll(ingredientIdQuery)
    if (len(res) == 0):
        return None
    return connectExecuteReturnFetchAll(ingredientIdQuery)[0][0]

# SQL Code to delete an ingredient to recipe relationship
def remove_ingredient_map(recipe_id, ingredient_id):
    removeIngredientMapQuery = """
        DELETE FROM IngredientMap WHERE recipe_id={} AND ingredient_id={};
    """.format(recipe_id, ingredient_id)
    connectExecuteSQL(removeIngredientMapQuery)

# Helper function to query ingredients pertaining to a certain recipe
def get_ingredients(recipe_id):
    getRecipeQuery = """
        SELECT Ingredients.ingredient_name, IngredientMap.quantity, IngredientMap.unit
        FROM IngredientMap INNER JOIN Ingredients ON IngredientMap.ingredient_id = Ingredients.id 
        AND IngredientMap.recipe_id = {};
    """.format(recipe_id)
    
    return connectExecuteReturnFetchAll(getRecipeQuery)

### ALL RECIPE RELATED QUERIES ###

# SQL Code Get all information pertaining to a recipe
def get_recipe_info(recipe_id):
    likeRatioQuery = """SELECT count(*) FROM Likes WHERE recipe = {} AND is_dislike is false;""".format(recipe_id)
    disLikeRatioQuery = """SELECT count(*) FROM Likes WHERE recipe = {} AND is_dislike is true;""".format(recipe_id)
    reviewCountQuery = """SELECT count(*) FROM Reviews WHERE recipe_id = {};""".format(recipe_id)
    recipeInformationQuery = """
        SELECT Users.first_name, Users.middle_initial, Users.last_name, Users.display_name,
        Recipes.recipe_name, Recipes.photo, Recipes.prep_time, Recipes.cook_time, Recipes.total_time, Recipes.recipe_description,
        RecipeGroup.group_name,
        NutritionalInformation.*
        FROM Recipes LEFT JOIN Users ON Users.id = Recipes.user_id
        LEFT JOIN RecipeGroup on RecipeGroup.id = Recipes.recipe_group_id
        LEFT JOIN NutritionalInformation ON NutritionalInformation.recipe_id = Recipes.id
        WHERE Recipes.id = {};
    """.format(recipe_id)
    instructionsQuery = "SELECT * FROM Instruction WHERE recipe_id={} ORDER BY step_number".format(recipe_id)

    like_ratio = connectExecuteReturnFetchAll(likeRatioQuery)
    dislike_ratio = connectExecuteReturnFetchAll(disLikeRatioQuery)
    instructions = connectExecuteReturnFetchAll(instructionsQuery)
    reviews_count = connectExecuteReturnFetchAll(reviewCountQuery)
    ingredients = get_ingredients(recipe_id)
    recipe_information = connectExecuteReturnFetchAll(recipeInformationQuery)

    return like_ratio, dislike_ratio, instructions, reviews_count, ingredients, recipe_information

# SQL code to create a new recipe group
def create_recipe_group(group_name):
    insertGroupQuery = """
        INSERT INTO RecipeGroup (group_name) VALUES ("{}");
    """.format(group_name)
    return connectExecuteInsertReturnLastRowIDSQL(insertGroupQuery)

# SQL Code to Select all recipe groups
def get_all_recipe_groups():
    fetchRecipeGroups = "SELECT * FROM RecipeGroup;"
    return connectExecuteReturnFetchAll(fetchRecipeGroups)

# SQL Code to Select recipe group by name
def get_recipe_group_by_name(name):
    fetchRecipeGroups = """
        SELECT * FROM RecipeGroup WHERE group_name LIKE "%{}%";
    """.format(name)
    return connectExecuteReturnFetchAll(fetchRecipeGroups)

# SQL Code to favourite a recipe
def favourite_recipe(user_ID, recipe_id):
    favouriteQuery = """
        INSERT INTO Favourites(user, recipe) VALUES ({}, {});
    """.format(user_ID, recipe_id)
    connectExecuteSQL(favouriteQuery)

# SQL Code to favourite a recipe group
def favourite_recipe_group(user_ID, group_id):
    favouriteQuery = """
        INSERT INTO RecipeGroupFollows (follower_id, group_id) VALUES ({}, {});
    """.format(user_ID, group_id)
    connectExecuteSQL(favouriteQuery)

# SQL Code To Create a Recipe
def create_recipe(user_ID, rgroupid, rname, rpfp, ptime, ctime, ttime, rdescription):
    createRecipeQuery = """
        INSERT INTO Recipes (
            user_id, 
            recipe_group_id, 
            recipe_name, 
            photo, 
            prep_time,
            cook_time,
            total_time,
            recipe_description
        )
        VALUES (
            {},
            {},
            "{}",
            "{}",
            {},
            {},
            {},
            "{}"
        )
    """.format(user_ID, rgroupid if rgroupid else "NULL", rname, rpfp, ptime, ctime, ttime, rdescription)
    return connectExecuteInsertReturnLastRowIDSQL(createRecipeQuery)

# Delete a recipe
def delete_recipe(user_ID, recipe_id):
    getRecipeAuthor = """
        SELECT user_id FROM Recipes WHERE id={};
    """.format(recipe_id)
    res = connectExecuteReturnFetchAll(getRecipeAuthor)
    if(len(res) == 0):
        print("Recipe not found")
        return
    else:
        uid = res[0][0]
        if str(uid) != user_ID:
            print("You are not the author of this recipe")
            return
    deleteRecipeQuery = """
        DELETE FROM Recipes WHERE id={} AND user_id={};
    """.format(recipe_id, user_ID)

    connectExecuteSQL(deleteRecipeQuery)
    print("Successfully deleted recipe")

# Search all recipes by limit of 10
def search_recipe():
    pageFetchQuery = """SELECT id, recipe_name FROM Recipes LIMIT 10;"""
    return connectExecuteReturnFetchAll(pageFetchQuery)

# Search recipes by an offset
def search_recipe_offset(offset):
    pageFetchQuery = """SELECT id, recipe_name FROM Recipes LIMIT 10 OFFSET {};""".format(offset)
    return connectExecuteReturnFetchAll(pageFetchQuery)

# Fetch all recipes given a recipe name
def fetch_recipe_by_name(rname):
    fetchQuery = """SELECT id, recipe_name FROM Recipes WHERE recipe_name LIKE '%{}%' ORDER BY recipe_name ASC;""".format(rname)
    res = connectExecuteReturnFetchAll(fetchQuery)
    if (len(res) == 0):
        return None
    return res

# Fetch all recipes by a specific author
def fetch_recipe_by_author(u_name):
    fetchQuery = """
        SELECT Recipes.id, Recipes.recipe_name FROM Recipes LEFT JOIN Users ON Recipes.user_id = Users.id WHERE Users.first_name LIKE '%{}%' OR Users.last_name LIKE '%{}%' OR Users.display_name LIKE '%{}%';
    """.format(u_name, u_name, u_name)
    res = connectExecuteReturnFetchAll(fetchQuery)
    if (len(res) == 0):
        return None
    return res

# Fetch all recipes pertaining to a recipe group
def fetch_recipe_by_group(recipe_group):
    fetchQuery = """
        SELECT Recipes.id, Recipes.recipe_name
        FROM RecipeGroup LEFT JOIN Recipes ON RecipeGroup.id = Recipes.recipe_group_id
        WHERE RecipeGroup.group_name LIKE "%{}%";
    """.format(recipe_group)
    res = connectExecuteReturnFetchAll(fetchQuery)
    if (len(res) == 0):
        return None
    return res

# Fetch all recipes under a certain time
def fetch_recipe_by_time(time):
    fetchQuery = """SELECT id, recipe_name FROM Recipes WHERE total_time<={};""".format(time)
    res = connectExecuteReturnFetchAll(fetchQuery)
    if (len(res) == 0):
        return None
    return res

# SQL Code for updating a recipe given a property
def update_recipe(recipe_id, prop, val, is_id):
    if prop is not None and val is not None:
        if is_id:
            updateRecipeQuery = """UPDATE Recipes SET {}={} WHERE id={};""".format(prop, val, recipe_id)
        else:
            updateRecipeQuery = """UPDATE Recipes SET {}="{}" WHERE id={};""".format(prop, val, recipe_id)
        connectExecuteSQL(updateRecipeQuery)

# SQL Code to Get a users favourite recipes
def get_favourite_recipes(user_id):
    fetchQuery = """
        SELECT Recipes.id, Recipes.recipe_name
        FROM Favourites LEFT JOIN Recipes ON Favourites.recipe = Recipes.id
        WHERE Favourites.user={};
    """.format(user_id)
    res = connectExecuteReturnFetchAll(fetchQuery)
    if (len(res) == 0):
        return None
    return res

### ALL Insturciton related Queries ###

# SQL Code to Insert an Instruction
def insert_new_instruction(recipeID, stepCounter, instruction):
    insertInstructionQuery = """
        INSERT INTO Instruction (
            recipe_id,
            step_number,
            instruction
        ) VALUES (
            {},
            {},
            "{}"
        )
    """.format(recipeID, stepCounter, instruction)

    connectExecuteSQL(insertInstructionQuery)

# SQL Code to list current instructions for recipe
def get_instructions(recipe_id):
    instructionsQuery = "SELECT * FROM Instruction WHERE recipe_id={} ORDER BY step_number".format(recipe_id)
    return connectExecuteReturnFetchAll(instructionsQuery)

# SQL Code to update an instruciton given a step number and recipe
def update_instruction(recipe_id, step_number, instruction):
    updateInstructionQuery = """
        UPDATE Instruction SET instruction="{}" WHERE recipe_id={} AND step_number={};
    """.format(instruction, recipe_id, step_number)
    connectExecuteSQL(updateInstructionQuery)

# SQL Code to delete an instruction
def delete_instruction(recipe_id, step_number):
    deleteInstructionQuery = """
        DELETE  FROM Instruction WHERE recipe_id={} AND step_number={}
    """.format(recipe_id, step_number)
    connectExecuteSQL(deleteInstructionQuery)

### ALL REVIEW RELATED QUERIES ###
def get_all_reviews(recipe_id):
    reviewQuery = """SELECT * FROM Reviews WHERE recipe_id = {};""".format(recipe_id)
    return connectExecuteReturnFetchAll(reviewQuery)

# Get reviews by a specific user
def get_reviews_by_user(reviewer_id):
    userReviewQuery = """
        SELECT Recipes.recipe_name, Reviews.rating, Reviews.review, Reviews.review_date, Recipes.id
        FROM Reviews LEFT JOIN Recipes ON Reviews.recipe_id = Recipes.id WHERE reviewer_id = {};""".format(reviewer_id)
    res = connectExecuteReturnFetchAll(userReviewQuery)
    if (len(res) == 0):
        return None
    return res

# Add a new review for recipe
def add_review(user_ID, recipe_id, rating, review):
    createReviewQuery = """
        REPLACE INTO Reviews (
            reviewer_id,
            recipe_id,
            rating,
            review
        ) VALUES (
            {},
            {},
            {},
            "{}"
        );
    """.format(user_ID, recipe_id, rating, review)
    connectExecuteSQL(createReviewQuery)

# Delete an existing review
def delete_review(user_ID, recipe_id):
    deleteReviewQuery = """
        DELETE FROM Reviews WHERE reviewer_id={} AND recipe_id={};
    """.format(user_ID, recipe_id)
    
    connectExecuteSQL(deleteReviewQuery)
### User additional feature queries ###
def like_recipe(user_ID, recipe_id, is_like):
    likeRecipeQuery = """
        INSERT INTO Likes (
            user,
            recipe,
            is_dislike
        ) VALUES (
            {},
            {},
            {}
        );
    """.format(user_ID, recipe_id, is_like)

    connectExecuteSQL(likeRecipeQuery)