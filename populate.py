import csv
import settings
import mysql.connector

conn = mysql.connector.connect(
    user=settings.USER,
    password=settings.PASSWORD,
    host=settings.HOST,
    database=settings.DATABASE
)

cursor = conn.cursor()

def get_minutes(time):
    time_list = time.split()
    total = 0
    if (len(time_list) == 1):
        return total
    for i in range(0, len(time_list), 2):
        if (time_list[i+1] == 'h'):
            total += (int(time_list[i]) * 60)
        else:
            total += (int(time_list[i]))
    return total

with open('clean_recipes.csv', newline='') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=';')
    next(csv_reader)
    for row in csv_reader:
        recipe_name = row[0]
        recipe_photo = row[2]
        full_name = row[3].split()
        prep_time = get_minutes(row[4])
        cook_time = get_minutes(row[5])
        total_time = get_minutes(row[6])
        
        ingredients = row[7].split(",")
        steps = row[8].split(".")

        # If users only have a display name default
        create_Users = """INSERT INTO ece356_recipes_g29.Users 
        (display_name)
        VALUES 
        (
            "{}"
        );""".format(full_name[0].replace("\"", "”"))

        # Split between first and last name if available
        if (len(full_name) > 1):
            first_name = full_name[0]
            last_name = full_name[1]
            create_Users = """
            INSERT INTO ece356_recipes_g29.Users 
            (first_name, last_name)
            VALUES 
            (
                "{}",
                "{}"
            );""".format(first_name.replace("\"", "”"), last_name.replace("\"", "”"))

        cursor.execute(create_Users)
        conn.commit()

        user_id = cursor.lastrowid

        create_Recipes = """INSERT INTO Recipes (
            user_id,
            recipe_name,
            photo,
            prep_time,
            cook_time,
            total_time
        ) VALUES (
            {},
            "{}",
            "{}",
            {},
            {},
            {}
        );""".format(user_id, recipe_name.replace("\"", "”"), recipe_photo.replace("\"", "”"), prep_time, cook_time, total_time)

        cursor.execute(create_Recipes)
        conn.commit()
        recipe_id = cursor.lastrowid
        
        for step_number in range(0, len(steps), 1):
            
            create_instructions = """INSERT INTO Instruction (
                recipe_id,
                step_number,
                instruction
            ) VALUES (
                {},
                {},
                "{}"
            );""".format(recipe_id, step_number + 1, steps[step_number].replace("\"", "”"))
            cursor.execute(create_instructions)
            conn.commit()
        
        for ingredient in ingredients:
            fetch_ingredient = """
            SELECT id FROM Ingredients 
            WHERE ingredient_name="{}";
            """.format(ingredient)
            
            cursor.execute(fetch_ingredient)
            ingredient_id = cursor.fetchone()
            if ingredient_id is not None:
                ingredient_id = ingredient_id[0]
            if ingredient_id is None:
                create_ingredient = """INSERT INTO Ingredients (
                    ingredient_name
                ) VALUES (
                    "{}"
                );""".format(ingredient)
                cursor.execute(create_ingredient)
                conn.commit()
                ingredient_id = cursor.lastrowid

            create_ingredient_map = """REPLACE INTO IngredientMap (
                recipe_id,
                ingredient_id
            ) VALUES (
                {},
                {}
            );""".format(recipe_id, ingredient_id)
            cursor.execute(create_ingredient_map)
            conn.commit()
