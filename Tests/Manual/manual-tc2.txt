jiamouwei@Jamess-MBP project % python3 client.py 
Enter your user ID or 'next' to skip:
next
Main Menu: What would you like to do? Type 'h' for help or 'quit' to quit:
hello
Main Menu: What would you like to do? Type 'h' for help or 'quit' to quit:
h
***List of Possible Commands***
sur......Set User (login)
cu.......Create User
crg......Create Recipe Group
fr.......Favourite Recipe
frg......Favourite Recipe Group
cr.......Create Recipe
fu.......Follow User
er.......Edit Recipe
dr.......Delete Recipe
su.......Search User
sr.......Search Recipe
lr.......Like Recipe
rr.......Review Recipe
drv......Delete Review
srg......Search Recipe Group
ur.......User Reviews
gfr......Get Favourite Recipes
gfu......Get Followed Users
Main Menu: What would you like to do? Type 'h' for help or 'quit' to quit:
drv
***Delete Existing Review***
Enter user ID:34
Enter recipe id:356
Main Menu: What would you like to do? Type 'h' for help or 'quit' to quit:
ur
Recipe Name: Golden Crescent Rolls Recipe  , Rating: 3 , Review:  fav lol so good , Date:  2021-12-23 11:30:52
Main Menu: What would you like to do? Type 'h' for help or 'quit' to quit:
ur
Recipe Name: Golden Crescent Rolls Recipe  , Rating: 3 , Review:  fav lol so good , Date:  2021-12-23 11:30:52
Main Menu: What would you like to do? Type 'h' for help or 'quit' to quit:
drv
***Delete Existing Review***
Enter recipe id:1
Main Menu: What would you like to do? Type 'h' for help or 'quit' to quit:
ur
No reviews found
Main Menu: What would you like to do? Type 'h' for help or 'quit' to quit:
crg
***Creating Recipe Group***
Enter a name for the recipe group: testgroup
Your recipe group ID is:  1
Main Menu: What would you like to do? Type 'h' for help or 'quit' to quit:
er
***Updating an Existing Recipe***
Enter recipe id: 1
Select the following to update: 
1. Recipe Group
2. Recipe Name
3. Photo
4. Preparation Time
5. Cook Time
6. Total Time
7. Recipe Description
8. Ingredients
9. Instructions/Steps
Type: q or 0 when finished
Please enter the number of the attribute you would like to update or (q) when finished updating: 1
Enter the new recipe group for the recipe to belong to: 1
Select the following to update: 
1. Recipe Group
2. Recipe Name
3. Photo
4. Preparation Time
5. Cook Time
6. Total Time
7. Recipe Description
8. Ingredients
9. Instructions/Steps
Type: q or 0 when finished
Please enter the number of the attribute you would like to update or (q) when finished updating: q
Main Menu: What would you like to do? Type 'h' for help or 'quit' to quit:
sr
***Search for Recipes***
Would you like to search for recipes using (1) View all recipes (2) Recipe Name, (3) User/Author, (4) Recipe Group, (5) Time Required: 4
Enter the name of the recipe group you would like to list recipes for: testgroup
---List all of recipes---
1 . ID:  1 , Recipe Name:  Golden Crescent Rolls Recipe 
Enter the id of the Recipe you would like to receive more information about or any key to quit: q

Main Menu: What would you like to do? Type 'h' for help or 'quit' to quit:
quit
jiamouwei@Jamess-MBP project % 