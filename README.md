# ECE356 Final Project

Done by Group 29, Fall 2021, 'Recipes' data set  
Video Presentation: https://clipchamp.com/watch/mwPAOjmM6m2

## Set Up
Note that the clean_recipes.csv file was used for this project. This comes from the first URL present in the Data Source section of the 02-recipes.pdf file.  

Clone this repository, maintain file structure
```bash
git clone https://git.uwaterloo.ca/j65wei/ece356-project.git
```
Run dependencies.sh to install necessary dependencies. Requires pip
```bash
source dependencies.sh
```

Our project uses a locally hosted MySQL database which can be found here:
```
https://www.mysql.com/
```

The file `settings.py` requires a HOST IP and the PORT to be ran on default port number 3306. Please update `settings.py` to the correct HOST variable if not running on localhost(127.0.0.1).To get Host IP Address the following command can be used
```mysql
mysql> SELECT SUBSTRING_INDEX(USER(), '@', -1) AS ip,  @@hostname as hostname, @@port as port, DATABASE() as current_database;
```


Create database schema. This will create a user called 'recipes_29'@'localhost' with all privileges. Password for this user is 'ece356'. Replace `{path_to_project}` with the directory path to this project
```mysql
mysql> source {path_to_project}/schema.sql
```

The following command requires `clean_recipes.csv` to be present in the same directory.

Populate with data from CSV. This command assumes the clean_recipes.csv file is in the same directory as populate.py
```bash
python3 populate.py 
```

## Usage
Before using the client, modify settings.py according to local database setup (port, host, etc). Please use 'recipes_29'@'localhost' user. 

Start client CLI
```bash
python3 client.py
```
Run Test Cases (where 'x' is test case number, [1-3])
```bash
source ./Tests/TestCase<x>/testCase<x>.sh
```