# ECE 356 Group 29, Fall 2021
# Ideal Client can be found in Ideal_Client.pdf
# Our proposed client solution includes the following CLI Commands:
# - Unfollowing Users
# - Unfavouriting recipes
# - View recipes of all users followed
# - View your liked and disliked recipes
# - Delete an ingredient

# The following code is the implemented client
from os import rename
import argparse
import server

# Global variable to keep track of current user.
# This is used to replicate a login functionality
user_ID = None

# CLI Command: Create User will add a new User to the Users Table
def createUser():
    global user_ID
    print("***Creating a new User***")
    fname = input('Enter user first name: ')
    mname = input ('Enter middle initial (optional): ')
    lname = input('Enter user last name: ')
    email = input('Enter user email: ')
    pfp = input('Enter link to user profile picture: ')
    displayname = input('Enter user display name: ')
    
    result = server.add_user(fname, mname, lname, email, pfp, displayname)
    user_ID = result
    print("Your New User ID is: ", result, ".")

# CLI Command: Create a new Recipe Group in RecipeGroup Table
def createRecipeGroup():
    print("***Creating Recipe Group***")
    group_name = input("Enter a name for the recipe group: ")
    result = server.create_recipe_group(group_name)
    print("Your recipe group ID is: ", result)

# CLI Command: Find recipe groups by name or view all in table
def searchRecipeGroup():
    print("***Searching for Recipe Groups***")
    opt = input("Would you like to display (1) All recipe groups, (2) Filter by recipe group names")
    if opt == 1:
        result = server.get_all_recipe_groups
        for row in result:
            print("ID: ", row[0], " Name: ", row[1])
    elif opt == 2:
        name = input("Enter name to match: ")
        result = server.get_recipe_group_by_name(name)
        for row in result:
            print("ID: ", row[0], " Name: ", row[1])
    else:
        print("Invalid option received")

# CLI Command: Add a specific recipe given an ID to a users favourite recipes list
def favouriteRecipe():
    global user_ID
    print("***Favourite Recipe***")

    if (user_ID is None):
        setUser()
    
    recipe_id = input("Enter the id of the recipe you would like to follow: ")
    server.favourite_recipe(user_ID, recipe_id)

# CLI Command: Add a specific recipe group for a user to follow as an ease of access
def favouriteRecipeGroup():
    global user_ID
    print("***Favourite Recipe Group***")

    if (user_ID is None):
        setUser()

    group_id = input("Enter the recipe group id you would like to follow: ")
    server.favourite_recipe_group(user_ID, group_id)

# CLI Command: Create a new Recipe in Recipes table
def createRecipe():
    global user_ID
    print("***Create Recipe***")

    if (user_ID is None):
        setUser()
    
    rname = input('Enter recipe name: ')
    rpfp = input('Enter link to recipe profile picture (optional): ') 
    rgroupid = input ('Enter recipe group ID (optional): ')
    ptime = input('Enter prep time in minutes: ')
    ctime = input('Enter cook time in minutes: ')
    ttime = input('Enter total time in minutes: ')
    rdescription = input('Enter recipe description: ')
    
    recipeID = server.create_recipe(user_ID, rgroupid, rname, rpfp, ptime, ctime, ttime, rdescription)

    print("Enter q at any time to finish entering ingredients.")
    while (True):
        ingredient = input("Enter ingredient name: ")
        if (ingredient == 'q'):
            break
        quantity = input("Enter ingredient quantity: ")
        if (quantity == 'q'):
            break
        unit = input("Enter ingredient quantity unit (g, ml, etc): ")
        if (unit == 'q'):
            break
        server.add_ingredient(recipeID, ingredient, quantity, unit)

    print('Enter q at any time to quit')
    stepCounter = 1
    while (True):
        instruction = input("Enter instruction for step "+str(stepCounter)+": ")
        if (instruction == 'q'):
            break
        server.insert_new_instruction(recipeID, stepCounter, instruction)
        stepCounter = stepCounter + 1       

    print("Your Recipe has been created with the ID: ", recipeID, ". Please remember this ID for any recipe related queries.") 

# CLI Command: Allows one user to follow/receive notifications about another user
def followUser():
    global user_ID
    print("***Following User***")
    follower_id = ""
    if (user_ID is None):
        setUser()
    else:
        follower_id = user_ID
    following_id = input("Enter the id of the user you want to follow: ")
    server.follow_user(follower_id, following_id)

# CLI Command: Update any existing attribute pertaining to a recipe given an ID
def updateRecipe():
    print("***Updating an Existing Recipe***")
    recipe_id = input('Enter recipe id: ')
    is_id = True
    while(True):
        print("Select the following to update: ")
        print("1. Recipe Group")
        print("2. Recipe Name")
        print("3. Photo")
        print("4. Preparation Time")
        print("5. Cook Time")
        print("6. Total Time")
        print("7. Recipe Description")
        print("8. Ingredients")
        print("9. Instructions/Steps")
        print("Type: q or 0 when finished")
        attr = input('Please enter the number of the attribute you would like to update or (q) when finished updating: ')
        if attr == 'q' or attr == 0:
            break
        prop = None
        val = None
        if attr == '1':
            group_id = input('Enter the new recipe group for the recipe to belong to: ')
            prop = 'recipe_group_id'
            val = group_id
        elif attr == '2':
            recipe_name = input('Enter new name for recipe: ')
            prop = 'recipe_name'
            val = recipe_name
            is_id = False
        elif attr == '3':
            photo = input('Enter new URL for photo: ')
            prop = 'photo'
            val = photo
            is_id = False
        elif attr == '4':
            pt = input('Enter new preparation time: ')
            prop = 'prep_time'
            val = pt
        elif attr == '5':
            ct = input('Enter new cook time: ')
            prop = 'cook_time'
            val = ct
        elif attr == '6':
            tt = input('Enter new total time: ')
            prop = 'total_time'
            val = tt
        elif attr == '7':
            description = input('Enter a new recipe description')
            prop = 'recipe_description'
            val = description
            is_id = False
        elif attr == '8':
            while (True):
                ingredients_record = server.get_ingredients(recipe_id)
                print("Ingredients: ")
                c = 1
                for row in ingredients_record:
                    quant = ""
                    if row[1] is not None:
                        quant = " " + str(row[1]) + " " + str(row[2])
                    print(c, ". ", row[0], quant, sep='')
                    c += 1
                opt = input('Would you like to (1)Delete an ingredient or (2)Add an ingredient from the list. Enter any other key to exit: ')
                if opt == '1':
                    ingredient_name = input("Enter the name of the ingredient you would like to remove: ")

                    ingredient_id = server.get_ingredient_id_by_name(ingredient_name)
                    if ingredient_id is not None:
                        print("Successfully removed ingredient", ingredient_name)
                        server.remove_ingredient_map(recipe_id, ingredient_id)
                elif opt == '2':
                    ingredient = input("Enter ingredient name: ")
                    if (ingredient == 'q'):
                        break
                    quantity = input("Enter ingredient quantity: ")
                    if (quantity == 'q'):
                        break
                    unit = input("Enter ingredient quantity unit (g, ml, etc): ")
                    if (unit == 'q'):
                        break
                    server.add_ingredient(recipe_id, ingredient, quantity, unit)
                else:
                    break

        elif attr == '9':
            count = 0
            print("Current list of instructions: ")
            instructions = server.get_instructions(recipe_id)
            for instruction in instructions:
                print(instruction[2], '. ', instruction[1])
            while(True):
                opt = input('Would you like to (1) Update an existing instruction (2) Add an instruction (3) Delete an instruction. Press any other key to quit: ')
                if opt == '1':
                    step = input('Enter current step: ')
                    instr = input('Enter new instruction: ')
                    server.update_instruction(recipe_id, step, instr)
                elif opt == '2':
                    instr = input('Enter the new instruction: ')
                    step = count + 1
                    server.insert_new_instruction(recipe_id, step, instr)
                elif opt == '3':
                    step = input('Which step number would you like to delete: ')
                    server.delete_instruction(recipe_id, step)
                else:
                    break
        else:
            print("Please enter a valid number")

        # Update recipe given a property and value
        server.update_recipe(recipe_id, prop, val, is_id)

# CLI Command: Delete a recipe from Recipes table given an ID
def deleteRecipe():
    global user_ID
    print("***Deleting an Existing Recipe***")

    if (user_ID is None):
        setUser()

    recipe_id = input('Enter recipe id: ')

    server.delete_recipe(user_ID, recipe_id)

# CLI Command: Find a User via first name, last name or display name
def searchUser():
    print("***Searching for a specific user's information***")
    opt = input("Would you like to search user using (1)First Name, (2)Last Name, (3)Display Name? ")
    result = None
    if opt == '1':
        first_name = input("Enter a first name: ")
        result = server.search_user('first_name', first_name)
    elif opt == '2':
        last_name = input("Enter a last name: ")
        result = server.search_user('last_name', last_name)
    elif opt == '3':
        display_name = input("Enter a display name: ")
        result = server.search_user('display_name', display_name)
    else:
        print("Invalid option received")
    if result is not None:
        for row in result:
            print("ID: ", row[0], ", First Name: ", row[1], ", Middle Name: ", row[2], ", Last Name: ", row[3], ", Email: ", row[4], ", Profile Picture: ", row[5], ", Display Name: ", row[6])
    
# Utility function: Return all information pertaining to a recipe
def getRecipeInfo(info_id):
    res = server.get_recipe_info(info_id)
    like_ratio = res[0]
    dislike_ratio = res[1]
    instructions = res[2]
    reviews_count = res[3]
    ingredients = res[4]
    recipe_information = res[5]

    ri = recipe_information[0]
    if ri[0]:
        print("Author:", ri[0], ri[1] if ri[1] else "", ri[2])
    else:
        print("Author: ", ri[3])
    print("Recipe Name: ", ri[4])
    if ri[9]:
        print("Recipe Description: ", ri[9])
    if ri[5]:
        print("Photo: ", ri[5])
    if ri[6]:
        print("Preparation Time: ", ri[6], "minutes")
    if ri[7]:
        print("Cook Time: ", ri[7], "minutes")
    if ri[8]:
        print("Total Time: ", ri[8], "minutes")
    if ri[10]:
        print("Recipe Group Name: ", ri[10])
    if ri[11]:
        print("--Nutritional Information--")
    if ri[13]:
        print("Calories: ", ri[13])
    if ri[14]:
        print("Protein: ", ri[14], "g")
    if ri[15]:
        print("Carbohydrates: ", ri[15], "g")
    if ri[16]:
        print("Fat: ", ri[16], "g")
    if ri[17]:
        print("Cholestral: ", ri[17], "mg")
    if ri[18]:
        print("Sodium: ", ri[18], "mg")
    if ri[19]:
        print("Sugar: ", ri[19], "g")

    print("Ingredients: ")
    for row in ingredients:
        print(row[0], end = '')
        if row[1] is not None:
            print(' ', row[1], end = '')
        if row[2] is not None:
            print(' ', row[2])
        else:
            print()
    print("Instructions: ")
    for instruction in instructions:
        print(instruction[2], '. ', instruction[1])
    print("Number of Reviews: ", reviews_count[0][0])

    print("Number of Likes: ", like_ratio[0][0])
    print("Number of Dislikes: ", dislike_ratio[0][0])

    opt_r = input("Would you like to read reviews (yes) or no to quit: ")
    if opt_r == "yes":
        reviews = server.get_all_reviews(info_id)
        if(len(reviews) > 0):
            print("Reviews: ")
            c = 1
            for review in reviews:
                print(c, ". Rating: ", review[3], ", Review: ", review[4], ", Date: ", review[5], sep='')
                c += 1
        else:
            print("No reviews found")
        rei = input("Would you like to add a review? (yes) or (no): ")
        if rei == "yes":
            reviewRecipe(info_id)

# CLI Command: Search recipe via user selected attributes
def searchRecipe():
    print("***Search for Recipes***")
    result = None
    opt = input("Would you like to search for recipes using (1) View all recipes (2) Recipe Name, (3) User/Author, (4) Recipe Group, (5) Time Required: ")
    if opt == '1':
        res = server.search_recipe()
        row_count = 1
        for row in res:
            print(row_count, ". ID: ", row[0], ", Recipe Name: ", row[1])
            row_count += 1
        page = 10
        while (True):
            page_option = input("Type 'n' to view next page. Type a recipe id for more information. Type 'q' to quit: ")
            if page_option == 'n':
                res = server.search_recipe_offset(page)
                for row in res:
                    print(row_count, ". ID: ", row[0], ", Recipe Name: ", row[1])
                    row_count += 1
                page = page + 10
            elif page_option == 'q':
                break
            else:
                try:
                    val = int(page_option)
                    getRecipeInfo(val)
                except ValueError:
                    print("Please enter a valid command.")
                break      
    elif opt == '2':
        rname = input("Enter a recipe name to search for: ")
        result = server.fetch_recipe_by_name(rname)

    elif opt == '3':
        u_name = input("Enter the first name, last name or display name of the users recipe you would like to search for: ")
        result = server.fetch_recipe_by_author(u_name)
    elif opt == '4':
        recipe_group = input("Enter the name of the recipe group you would like to list recipes for: ")
        result = server.fetch_recipe_by_group(recipe_group)
    elif opt == '5':
        time = input("Enter the maximum total time: ")
        result = server.fetch_recipe_by_time(time)
    else:
        print("Please enter a valid command")
    if result is not None:
        print("---List all of recipes---")
        row_count = 1
        for row in result:
            print(row_count, ". ID: ", row[0], ", Recipe Name: ", row[1])
            row_count += 1

        extra_info = input("Enter the id of the Recipe you would like to receive more information about or any key to quit: ")
        try:
            val = int(extra_info)
            getRecipeInfo(val)
        except ValueError:
            print("")
    else:
        print("No recipes found")

# CLI Command: Allows a user to either upvote or downvote a recipe
def likeRecipe():
    global user_ID
    print("***Like a specific recipe***")

    if (user_ID is None):
        setUser()

    recipe_id = input('Enter recipe id: ')
    is_like = input('Would you like to upvote(like) or downvote(dislike): ')

    if (is_like == 'like'):
        is_like = False
    else:
        is_like = True
    server.like_recipe(user_ID, recipe_id, is_like)

# CLI Command: Allows user to create a review for a recipe and give rating out of 5
def reviewRecipe(recipe_id):
    global user_ID
    print("***Add Review for Recipe***")

    if (user_ID is None):
        setUser()

    if recipe_id is None:
        recipe_id = input('Enter recipe id (WARNING: This will overwrite previous reviews for this recipe): ')
    rating = input('Enter rating 1-5 (whole number): ')
    review = input('Enter a short comment about the recipe: ')

    server.add_review(user_ID, recipe_id, rating, review)

# CLI Command: Delete an existing recipe
def deleteReview():
    global user_ID
    print("***Delete Existing Review***")

    if (user_ID is None):
        user_ID = input('Enter user ID:')
    
    recipe_id = input('Enter recipe id:')
    server.delete_review(user_ID, recipe_id)

# CLI Command: List all reviews posted by a user
def userReviews():
    global user_ID
    if (user_ID is None):
        user_ID = input('Enter user ID:')
    result = server.get_reviews_by_user(user_ID)
    if result is not None:
        print("---List all of Reviews---")
        for row in result:
            print("Recipe ID: ", row[4], ", Recipe Name:", row[0], ", Rating:", row[1], ", Review: ", row[2], ", Date: ", row[3])
    else:
        print("No reviews found")

# CLI Command: Get a list of recipes favourited
def getFavouriteRecipes():
    global user_ID
    if (user_ID is None):
        user_ID = input('Enter user ID: ')
    result = server.get_favourite_recipes(user_ID)
    if result is not None:
        print("---List all of recipes---")
        row_count = 1
        for row in result:
            print(row_count, ". ID: ", row[0], ", Recipe Name: ", row[1])
            row_count += 1

        extra_info = input("Enter the id of the Recipe you would like to receive more information about or any key to quit: ")
        try:
            val = int(extra_info)
            getRecipeInfo(val)
        except ValueError:
            print("")
    else:
        print("No recipes found")
    
# CLI Command: Get a list of users followed
def getFollowedUsers():
    global user_ID
    if (user_ID is None):
        user_ID = input('Enter user ID: ')
    result = server.get_followed_users(user_ID)
    if result is not None:
        print("---List all of followed users---")
        row_count = 1
        for row in result:
            name = ""
            if row[0] is not None:
                name = row[0]
                if row[1]:
                    name += " " + row[1]
                if row[2]:
                    name += " " + row[2]
            else:
                name = row[3]
            print(row_count, ".", name)
            row_count += 1
    else:
        print("No followed users found")

# Helper function to set a "login" for a user
def setUser():
    global user_ID
    user_ID = input("Enter your user ID: ")
    res = server.get_user_by_id(user_ID)
    if (len(res) > 0):
        row = res[0]
        name = ""
        if row[0] is not None:
            name = str(row[0])
            name += " "
            if row[1]:
                name += str(row[1])
            if row[2]:
                name += str(row[2])
        else:
            name = row[3]
        print("Hello", name)
    else:
        print("User not found with id:", user_ID)

    
# Utility function to print list of available commands
def printHelp():
    print("***List of Possible Commands***")
    print("sur......Set User (login)")
    print("cu.......Create User")
    print("crg......Create Recipe Group")
    print("fr.......Favourite Recipe")
    print("frg......Favourite Recipe Group")
    print("cr.......Create Recipe")
    print("fu.......Follow User")
    print("er.......Edit Recipe")
    print("dr.......Delete Recipe")
    print("su.......Search User")
    print("sr.......Search Recipe")
    print("lr.......Like Recipe")
    print("rr.......Review Recipe")
    print("drv......Delete Review")
    print("srg......Search Recipe Group")
    print("ur.......User Reviews")
    print("gfr......Get Favourite Recipes")
    print("gfu......Get Followed Users")

# MAIN FUNCTION CALL
if __name__ == '__main__':

    user_ID = input("Enter your user ID or 'next' to skip:\n")
    if (user_ID == 'next'):
        user_ID = None

    while (True):
        cmd = input("Main Menu: What would you like to do? Type 'h' for help or 'quit' to quit:\n")

        if (cmd == 'cu'):
            createUser()
        elif (cmd == 'sur'):
            setUser()
        elif (cmd == 'crg'):
            createRecipeGroup()
        elif (cmd == 'fr'):
            favouriteRecipe()
        elif (cmd == 'frg'):
            favouriteRecipeGroup()
        elif (cmd == 'cr'):
            createRecipe()
        elif (cmd == 'fu'):
            followUser()
        elif (cmd == 'er'):
            updateRecipe()
        elif (cmd == 'dr'):
            deleteRecipe()
        elif (cmd == 'su'):
            searchUser()
        elif (cmd == 'sr'):
            searchRecipe()
        elif (cmd == 'lr'):
            likeRecipe()
        elif (cmd == 'rr'):
            reviewRecipe(None)
        elif (cmd == 'drv'):
            deleteReview()
        elif (cmd == 'srg'):
            searchRecipeGroup()
        elif (cmd == 'ur'):
            userReviews()
        elif (cmd == 'gfr'):
            getFavouriteRecipes()
        elif (cmd == 'gfu'):
            getFollowedUsers()
        elif (cmd == 'h'):
            printHelp()
        elif (cmd =='quit'):
            break